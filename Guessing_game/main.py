from random import randint, random

print("Игра Угадайка", "Введите числа от 1 до 1000", sep = '\n')
number = randint(1, 1000)
n = input()

def is_valid(num):                      #создаем функцию для провверки числа
    for i in range(3):
        if num.isalpha() or num in '!?:.,/@#№$;%^&*()-_=+':
            print('Введите число,а не буквы')
            num = input()
        else:
            if num.isdigit():
                num = int(num)
                if num > 1000:
                    print('Введите число меньше 1000')
                    num = input()
                elif num < 0:
                    print('Введите число больше 0')
                    num = input()
                elif 0 < num < 1000:
                    break
            else:
                print('Введите только число!')
                num = input()


    if num == str(num) or num < 0 or num > 1000:
        num = 'Вы вводили неправильные числа, игра завершилась.'
    global n
    n = num
    return num


def correct_number():
    global n
    count = 0
    if n == str(n):
        return count
    for i in range(2, 1000):
        if number == n:
            print('Поздравляем! Вы угадали число, игра окончена.')
            break
        elif number > n:
            print('Больше числа', n)
            count += 1
            n = input()
            is_valid(n)
        elif number < n:
            print('Меньше числа', n)
            count += 1
            n = input()
            is_valid(n)
    return count

is_valid(n)
print(f'Кол-во ваших попыток {correct_number()}', f'Число которое нужно было угадать {number}', sep = '\n')


